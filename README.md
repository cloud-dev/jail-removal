Removal of autojails from surf maps
-------------------------------------------------------------------------------
This is currently a shit script from a while ago that was written in seconds
to be a removeall for all logic timers or triggers that would send players to
a jail. 

This will never be updated, as I have implemented this in a much better way.

I don't recommend actually using this. It is here simply so people unaware of
how to fix/remove jails in lua will have a place to start.

-------------------------------------------------------------------------------
You can place this in /lua/autorun/server to run the script when the map loads

It will remove all entities matching:

-trigger_once

-trigger_multiple

-logic_timer

This only occurs when the current mapname matches one from the if condition.