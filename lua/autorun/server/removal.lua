hook.Add( "InitPostEntity", "RemoveJailTrigger", function()
	if game.GetMap() == surf_refraxis or game.GetMap() == "surf_annoyance_njv" or game.GetMap() == "surf_sunset2_fix" or game.GetMap() == "surf_fruits" or game.GetMap() == "surf_eclipse" or game.GetMap() == "surf_ing" or game.GetMap() == "surf_forbidden_tomb" or game.GetMap() == "surf_forbidden_tomb2" or game.GetMap() == "surf_forbidden_tomb4" or game.GetMap() == "surf_overgrowth" then
		local e = ents.FindByClass( "trigger_once" )
		for _,v in pairs(e) do if IsValid(v) then v:Remove() end end
   
		e = ents.FindByClass( "trigger_multiple" )
		for _,v in pairs(e) do if IsValid(v) then v:Remove() end end
   
		e = ents.FindByClass( "logic_timer" )
		for _,v in pairs(e) do if IsValid(v) then v:Remove() end end
	end
end )